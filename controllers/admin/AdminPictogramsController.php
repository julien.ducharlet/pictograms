<?php
require_once _PS_MODULE_DIR_ . '/pictograms/classes/PictogramGroup.php';
    class AdminPictogramsController extends ModuleAdminController {
        public function __construct()
        {
            // variable definition
            $this->table = 'pictogram_group_lang';
            $this->identifier = 'id_pictogram_group';
            $this->className = 'PictogramGroup';

            // call parent construct
            parent::__construct();

            $this->fields_list = array(
                'id_pictogram_group' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
                'name_pictogram_group_lang' => array('title' => $this->l('Nom'), 'width' => 120),
            );

            // activate Bootstrap
            $this->bootstrap = true;



            $this->addRowAction('edit');
            $this->addRowAction('delete');
        }
    }