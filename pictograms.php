<?php
// security : if the Prestashop constant (version number) does not exists => stops the module from loading
if (!defined('_PS_VERSION_')) {
    exit;
}

require_once _PS_MODULE_DIR_ . '/pictograms/classes/PictogramGroup.php';

// MAIN CLASS (extends the Module class)
class Pictograms extends Module
{
    // constructor, first method called when the module is loaded by Prestashop
    public function __construct()
    {
        // internal identifier (same name than the module's folder)
        $this->name = 'pictograms';
        // category of the module in the back office modules list
        $this->tab = 'content_management';
        // version of the module
        $this->version = '1.0.0';
        // author of the module
        $this->author = 'Piment Bleu';
        // load the module's class when displaying the "Modules" pages in back office ? No : 0 / Yes : 1
        // exemple : to display a warngin message in the "Modules" page, set to 1
        $this->need_instance = 0;
        // compatibility of the module with
        // max not included !
        // min 1.5 / max 1.6 => the module works only in 1.5.x
        $this->ps_versions_compliancy = [
            'min' => '1.6',
            'max' => _PS_VERSION_
        ];
        // module's template files built with Prestashop bootstrap tool's in mind
        // Prestashop should not try to wrap the template code for the configuration screen with helper tags
        $this->bootstrap = true;
        // trigger the constructor of the Modules class
        // to use after the creation of "this->name = variable"
        // and before any use of the "this->l()" translation method
        parent::__construct();
        // name for the module, displayed in the back office's module list
        $this->displayName = $this->l('Pictograms');
        // description of the module, displayed in the back office's modules list
        $this->description = $this->l('Add pictograms in your products pages');
        // message displayed to the administrator, asking if he really want to uninstall the module
        // to be used in the installation code
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall pictograms ?');
        
        // exemple : warning message : if the module doesn't have its "MYMODULE_NAME" database value set yet
        /*if (!Configuration::get('MYMODULE_NAME')) {
            $this->warning = $this->l('No name provided');
        }*/
        
    }
    // control what happens when the store administrator install the module
    // here we can create SQL tables, copy files, create configuration variables, etc
    public function install()
    {
        // if the file install.php in sql exists => include this file
        // CREATE SQL tables (if not exists)
        if ((file_exists($this->local_path.'sql/install.php'))
        && ($this->installTab('AdminCatalog', 'AdminPictograms', 'Pictogrammes')))
        {
            // && (file_exists($this->local_path.'sql/insert_datas_dev.php')))
            include($this->local_path.'sql/install.php');
            include($this->local_path.'sql/insert_datas_dev.php');
        }
        else {
            return false;
        }

        // optional ? if the multistore feature is enabled => set the current context to all shops in the installation of PrestaShop
        /*if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }*/
        
        // return true if module correctly installed, or false otherwise
        // check that the module can be attached to the left column hook
        // check if the module can be attached to the header hook
        // TO DO : check if can be attached to other hooks ?
        return parent::install();
            // && $this->registerHook('leftColumn')
            // && $this->registerHook('header');
            // exemple : create the module MYMODULE_NAME configuration setting, and set its value to "my friend"
            // && Configuration::updateValue('MYMODULE_NAME', 'my friend')
    }
    // control what happens when the store administrator uninstall the module
    // here we can drop SQL tables, delete files, delete configuration variables, etc
    public function uninstall()
    {
        // if the file uninstall.php in sql exists => include this file
        // DROP SQL tables
        if ((file_exists($this->local_path.'sql/uninstall.php'))
        && ($this->uninstallTab('AdminPictograms')))
        {
            include($this->local_path.'sql/uninstall.php');
        } else {
            return false;
        }

        // return true if module correctly uninstalled, or false otherwise
        // check that the module can be detached to the left column hook
        // check if the module can be detached to the header hook
        // TO DO : check if can be detached to other hooks ?
        return parent::uninstall();
        // && $this->unregisterHook('leftColumn')
        // && $this->unregisterHook('header');
            // exemple : delete the data added to the database during the installation (MYMODULE_NAME configuration setting)
            // && Configuration::deleteByName('MYMODULE_NAME')
    }

    public function installTab($parent, $class_name, $name)
    {
        // create a new admin tab
        $tab = new Tab();
        $tab->id_parent = (int)Tab::getIdFromClassName($parent);
        $tab->name = array();
        // for trad :
        /* $tab->name = array(
            'en' => 'Pictograms'
            'fr' => 'Pictogrammes'
        );*/
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = $name;
        }
        $tab->class_name = $class_name;
        $tab->module = $this->name;
        $tab->active = 1;
        return $tab->add();
    }

    public function uninstallTab($class_name)
    {
        // get the id of the administration tab
        $id_tab = (int)Tab::getIdFromClassName($class_name);

        // Load tab
        $tab = new Tab((int)$id_tab);

        // delete tab
        return $tab->delete();
    }

    ////////////////////// UNUSED /////////////////////////////////////
    /*public function getContent()
    {
        $output = null;
        
        if (Tools::isSubmit('submit'.$this->name)) {
            $myModuleName = strval(Tools::getValue('MYMODULE_NAME'));

            if (
                !$myModuleName ||
                empty($myModuleName) ||
                !Validate::isGenericName($myModuleName)
            ) {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            } else {
                Configuration::updateValue('MYMODULE_NAME', $myModuleName);
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }
        return $output.$this->displayForm();
    }
    public function displayForm()
    {
        // Get default language
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');
        
        // Init Fields form array
        $fieldsForm[0]['form'] = [
            'legend' => [
                'title' => $this->l('Settings'),
            ],
            'input' => [
                [
                    'type' => 'text',
                    'label' => $this->l('Configuration value'),
                    'name' => 'MYMODULE_NAME',
                    'size' => 20,
                    'required' => true
                ]
            ],
            'submit' => [
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            ]
        ];
    
        // Helper
        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = [
            'save' => [
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ],
            'back' => [
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ]
        ];

        // Load current value
        $helper->fields_value['MYMODULE_NAME'] = Configuration::get('MYMODULE_NAME');

        return $helper->generateForm($fieldsForm);
    }
    public function hookDisplayLeftColumn($params)
    {
        // $this->context : change Smarty's assign() method
        // to set the template's name variable with the value of the MYMODULE_NAME setting stored in the configuration database table
        $this->context->smarty->assign([
            'my_module_name' => Configuration::get('MYMODULE_NAME'),
            'my_module_link' => $this->context->link->getModuleLink('mymodule', 'display')
        ]);
        
        return $this->display(__FILE__, 'mymodule.tpl');
    }
    public function hookDisplayRightColumn($params)
    {
        return $this->hookDisplayLeftColumn($params);
    }
    // Header hook : enables to put code in the <head> tag of the generated HTML file
    // addCSS to add a <link> tag to the CSS file indicated in parameters
    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'css/mymodule.css', 'all');
    }*/
}