<?php
class PictogramGroup extends ObjectModel
{
    public $id_pictogram_group_lang;
    public $name_pictogram_group_lang;

    public static $definition = array(
        'table' => 'pictogram_group_lang',
        'primary' => 'id_pictogram_group',
        'multilang' => false,
        'fields' => array(
            '$id_pictogram_group' => array('type' => self::TYPE_INT,
                                        'validate' => 'isUnsigned',
                                        'required' => true),
            /*'position_pictogram_group' => array('type' => self::TYPE_INT,
                                    'validate' => 'isUnsignedId',
                                    'required' => true),*/
                ),
        );
}