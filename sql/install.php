<?php
// security : if the Prestashop constant (version number) does not exists => stops the module from loading
if (!defined('_PS_VERSION_')) {
    exit;
}

$sql = array(
    'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogram_group` (
        `id_pictogram_group` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `position_pictogram_group` INT(11) UNSIGNED NOT NULL,
        CONSTRAINT `'._DB_PREFIX_.'pictogram_group_pk` PRIMARY KEY (`id_pictogram_group`)
    ) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8;',
    
    'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogram_group_lang` (
        `id_pictogram_group` INT(11) UNSIGNED NOT NULL,
        `id_lang` INT(11) UNSIGNED NOT NULL,
        `name_pictogram_group_lang` VARCHAR(255) NOT NULL,
        CONSTRAINT `'._DB_PREFIX_.'pictogram_group_lang_pk` PRIMARY KEY (`id_pictogram_group`,`id_lang`),
        CONSTRAINT `'._DB_PREFIX_.'pictogram_group_lang_fk_1` FOREIGN KEY (`id_pictogram_group`) REFERENCES `'._DB_PREFIX_.'pictogram_group` (`id_pictogram_group`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8;',
    
    'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogram_group_shop` (
        `id_pictogram_group` INT(11) UNSIGNED NOT NULL,
        `id_shop` INT(11) UNSIGNED NOT NULL,
        CONSTRAINT `'._DB_PREFIX_.'pictogram_group_shop_pk` PRIMARY KEY (`id_pictogram_group`, `id_shop`),
        CONSTRAINT `'._DB_PREFIX_.'pictogram_group_shop_fk_1` FOREIGN KEY (`id_pictogram_group`) REFERENCES `'._DB_PREFIX_.'pictogram_group` (`id_pictogram_group`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE='._MY_SQL_ENGINE.' DEFAULT CHARSET=utf8;',
    
    'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogram` (
        `id_pictogram` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `id_pictogram_group` INT(11) UNSIGNED NOT NULL,
        `position_pictogram` INT(11) UNSIGNED NOT NULL,
        CONSTRAINT `'._DB_PREFIX_.'pictogram_pk` PRIMARY KEY (`id_pictogram`),
        CONSTRAINT `'._DB_PREFIX_.'pictogram_fk_1` FOREIGN KEY (`id_pictogram_group`) REFERENCES `'._DB_PREFIX_.'pictogram_group` (`id_pictogram_group`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8;',
    
    'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogram_lang` (
        `id_pictogram` INT(11) UNSIGNED NOT NULL,
        `id_lang` INT(11) UNSIGNED NOT NULL,
        `name_pictogram_lang` VARCHAR(128) NOT NULL,
        `description_pictogram_lang` VARCHAR(255),
        CONSTRAINT `'._DB_PREFIX_.'pictogram_lang_pk` PRIMARY KEY (`id_pictogram`,`id_lang`),
        CONSTRAINT `'._DB_PREFIX_.'pictogram_lang_fk_1` FOREIGN KEY (`id_pictogram`) REFERENCES `'._DB_PREFIX_.'pictogram` (`id_pictogram`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE='._MY_SQL_ENGINE_.' DEFAULT CHARSET=utf8;',
    
    'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pictogram_shop` (
        `id_pictogram` INT(11) UNSIGNED NOT NULL,
        `id_shop` INT(11) UNSIGNED NOT NULL,
        CONSTRAINT `'._DB_PREFIX_.'pictogram_shop_pk` PRIMARY KEY (`id_pictogram`, `id_shop`),
        CONSTRAINT `'._DB_PREFIX_.'pictogram_shop_fk_1` FOREIGN KEY (`id_pictogram`) REFERENCES `'._DB_PREFIX_.'pictogram` (`id_pictogram`) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE='._MY_SQL_ENGINE.' DEFAULT CHARSET=utf8;'
);

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}